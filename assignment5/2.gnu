clear
reset
set key off
set border 3
set xrange [0:50]
#set terminal eps nocrop enhanced size 1366,768
#set output 'data_2.png'

set term postscript eps enhanced 24

set output 'data_2.eps'

set xtics 5
# Add a vertical dotted line at x=0 to show centre (mean) of distribution.
set yzeroaxis

# Each bar is half the (visual) width of its x-range.
set boxwidth 0.9 absolute
set style fill solid 1.0 noborder

set style fill pattern 2
set style histogram columnstacked

bin_width = 1;

bin_number(x) = floor(x/bin_width)

rounded(x) = bin_width * ( bin_number(x) + 0 )

unset label
set label "n = 50" at 40,100
set label "p = 0.3" at 40,80
set title "Binomial Distribution over 1000 samples"
plot 'data_2_B_1000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "{/Symbol l} = 15" at 40,80
set title "Poission Distribution over 1000 samples"
plot 'data_2_P_1000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "{/Symbol m} = 15" at 40,100
set label "{/Symbol s} = {/Symbol @{\140\140\140\140}\326}10.5" at 40,80
set title "Guassian Distribution over 1000 samples"
plot 'data_2_G_1000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes


unset label
set label "n = 50" at 40,1000
set label "p = 0.3" at 40,800
set title "Binomial Distribution over 10000 samples"
plot 'data_2_B_10000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "{/Symbol l} = 15" at 40,800
set title "Poission Distribution over 10000 samples"
plot 'data_2_P_10000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "{/Symbol m} = 15" at 40,1000
set label "{/Symbol s} = {/Symbol @{\140\140\140\140}\326}10.5" at 40,800
set title "Guassian Distribution over 10000 samples"
plot 'data_2_G_10000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes


unset label
set label "n = 50" at 40,10000
set label "p = 0.3" at 40,8000
set title "Binomial Distribution over 100000 samples"
plot 'data_2_B_100000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "{/Symbol l} = 15" at 40,8000
set title "Poission Distribution over 100000 samples"
plot 'data_2_P_100000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "{/Symbol m} = 15" at 40,10000
set label "{/Symbol s} = {/Symbol @{\140\140\140\140}\326}10.5" at 40,8000
set title "Guassian Distribution over 100000 samples"
plot 'data_2_G_100000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "n = 50" at 40,100000
set label "p = 0.3" at 40,80000
set title "Binomial Distribution over 1000000 samples"
plot 'data_2_B_1000000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "{/Symbol l} = 15" at 40,80000
set title "Poission Distribution over 1000000 samples"
plot 'data_2_P_1000000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes

unset label
set label "{/Symbol m} = 15" at 40,100000
set label "{/Symbol s} = {/Symbol @{\140\140\140\140}\326}10.5" at 40,80000
set title "Guassian Distribution over 1000000 samples" 
plot 'data_2_G_1000000.mat' i 0 using (rounded($1)):(1.0) smooth frequency with boxes
