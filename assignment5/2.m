n = 50
p = 0.3
l = 15
u = 15
s = sqrt(10.5)

nn = 1000

B = binornd(n,p,nn,1)
P = poissrnd(l,nn,1)
G = normrnd(u,s,nn,1)

save data_2_B_1000.mat B
save data_2_P_1000.mat P
save data_2_G_1000.mat G

nn = 10000

B = binornd(n,p,nn,1)
P = poissrnd(l,nn,1)
G = normrnd(u,s,nn,1)

save data_2_B_10000.mat B
save data_2_P_10000.mat P
save data_2_G_10000.mat G

nn = 100000

B = binornd(n,p,nn,1)
P = poissrnd(l,nn,1)
G = normrnd(u,s,nn,1)

save data_2_B_100000.mat B
save data_2_P_100000.mat P
save data_2_G_100000.mat G

nn = 1000000

B = binornd(n,p,nn,1)
P = poissrnd(l,nn,1)
G = normrnd(u,s,nn,1)

save data_2_B_1000000.mat B
save data_2_P_1000000.mat P
save data_2_G_1000000.mat G
