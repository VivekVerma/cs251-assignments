#!usr/bin/gnuplot
set term postscript eps enhanced color 24
set output 'graph_1.eps'
set xlabel "x"
set ylabel "Function"
set title "Distribution Functions"
set key top right
set label "n = 50" at 35,0.10
set label "p = 0.3" at 35,0.08
set label "{/Symbol l} = 15" at 35,0.06
set label "{/Symbol m} = 15" at 35,0.04
set label "{/Symbol s} = {/Symbol @{\140\140\140\140}\326}10.5" at 35,0.02
plot 'data_1.mat' u 1:2 title "Binomial" w lp,'data_1.mat' u 1:3 title "Poission" w lp,'data_1.mat' u 1:4 title "Gaussian" w lp
