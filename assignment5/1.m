n = 50
p = 0.3
l = 15
u = 15
s = sqrt(10.5)
A = zeros(n,4)
for i = 1:n
    A(i,1) = i - 1;
    A(i,2) = binopdf(A(i,1),n,p);
    A(i,3) = poisspdf(A(i,1),l);
    A(i,4) = normpdf(A(i,1),u,s);
end
save data_1.mat A
