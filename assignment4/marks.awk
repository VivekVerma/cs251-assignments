#!/usr/bin/awk -f
#awk -f ./marks.awk input > output

BEGIN{
	p = -1;
	minn = -log(0);
}

{
	if(NR==1){
	
		for(i=5;i<=NF;i+=2)
		MIN[(i-5)/2] = 100;
	}

	for(i=5;i<=NF;i+=2){
		t+=$i;
		q = (i-5)/2;
		SUMc[q] += $i;
		SUMsq[q] += $i*$i;
		MAX[q] = (MAX[q]<$i) ? $i : MAX[q];
		MIN[q] = ($i<MIN[q]) ? $i : MIN[q];
	}
	p+=1;
	SUMr[p] = t;
	summ+=t;
	summsq+=t*t;
	if(maxx<t) maxx=t;
	if(t<minn) minn=t;
	printf "%s |%6d\n", $0, t;
	t=0;
}

END{
printf "max    |       |";
for(i=5;i<=NF;i+=2)
printf "%6d |",MAX[(i-5)/2];
printf "%6d",maxx;

printf "\n";

printf "min    |       |";
for(i=5;i<=NF;i+=2)
printf "%6d |",MIN[(i-5)/2];
printf "%6d",minn;

printf "\n";

printf "mean   |       |";
for(i=5;i<=NF;i+=2)
printf "%6.2f |",SUMc[(i-5)/2]/NR;
printf "%6.2f",summ/NR;

printf "\n";

printf "sd     |       |";
for(i=5;i<=NF;i+=2)
printf "%6.2f |",sqrt(SUMsq[(i-5)/2]/NR-(SUMc[(i-5)/2]/NR)*(SUMc[(i-5)/2]/NR));
printf "%6.2f",sqrt(summsq/NR-(summ/NR)*(summ/NR));
}
